// Libraries
console.log("Loading libraries...");

var net = require('net');
var _ = require('underscore');
var modbus = require('modbus').create();
var nmodbus = require('modbus').native;
var engine = require('engine.io');

console.log("Done.");


// Configuration
var config = require('./config.json');


// Locks
var modbusReady = false;
var selfRequests = true;
var selfRequestTimer;
var selfRequestPointer=0;
var selfRequestInProgress=false;
var wariTimeout;

// Datapoint Factory
var datapoints = require('./datapoints');

// ECU Connection using Serial Port
var modbusConnection = modbus.createMaster({
	con: modbus.createConRtu(1, config.serialPort, 57600),

	onConnect: function(){
		console.log("Modbus connected.");

		//timeouts = { tv_sec: 2, tv_usec: 0 }
		//nmodbus.set_response_timeout(modbusConnection.getContext(), timeouts)
		
		selfRequestTimer = setTimeout(fetchEcuData, config.interval);
		modbusReady = true;
	},

	onDestroy: function(){
		modbusReady = false;
		clearTimeout(selfRequestTimer);
	}

});

// SetTimeout for modbusReady to be True, or quit presuming that the USB device is locked / not working or some shit
setTimeout(function checkMBConnection(){
	if (modbusReady == false) {
		throw new Error('Modbus is not connected yet - an error may have occurred.');
	}
}, 5000);


// TCP Listener for WARI to connect to
var server = net.createServer(function(socket){
	console.log("Connection from client, self requests are disabled. (Unless timeout occurs.)");
	selfRequests = false;
	
	socket.on('error', function(err){
		console.log("Socket error..");
		console.log(err.stack);
	});

	socket.on('end', function(){
		console.log("Connection closed from client. Enabling self requests.");
		selfRequests = true;
	});

	socket.on('data', function(request){

		// Pick the message from the socket
		var forwardedMessage = request.slice(0, request.length - 2);
		if (!modbusReady) {
			console.log("Modbus is not ready for this yet.")
			return;
		}

		if (selfRequests) {
			console.log("Self requests are enabled. Argh.. Fights for who-requests-what?!!");
			return;
		}
		
		// Native modbus request - ctx, message, length
		nmodbus.send_raw_request(modbusConnection.getContext(), forwardedMessage, forwardedMessage.length);
		
		// Get the response
		var response = [];
		var respCnt = nmodbus.receive_confirmation(modbusConnection.getContext(), response);

		
		// Convert response into a Buffer
		response = new Buffer(response);

		if (respCnt != -1) {
			socket.write(response);
		}

		// Pickup the pieces of what happened...
		
		var packet = requestDisassembler(request);
		if (packet['unit'] == 1 && packet['func'] == 3) {
			var responsePacket = responseDisassembler(response, packet['count']);
			
			if (responsePacket) {
				// Forward to the response handler to xmit
				responseHandler(responsePacket['registers'], packet['start']);
			}
		}

	});

}).listen(config.wariPort, function(){
	console.log("Listening for WARI requests on port:", config.wariPort);
});



// Websocket front end for client device to watch
wsServer = engine.listen(config.wsPort, function(){
	console.log("Listening for WS connections on port:", config.wsPort);
});

// Send message to everyone but (optionally) the sending client
wsServer.broadcast = function(mssg, id) {
	for( var key in wsServer.clients ) {
		if(typeof id !== 'undefined') {
			// Don't broadcast to sending client
			if( key == id ) {
				continue;
			}
		}
		wsServer.clients[key].send(mssg);
	}
}



// Request helper functions
function requestDisassembler(request) {
	var packet = {};
	if (!request) {
		return packet;
	}

	packet.unit = request.readUInt8(0, true);
	packet.func = request.readUInt8(1, true);
	packet.start = request.readUInt16BE(2, true);
	packet.count = request.readUInt16BE(4, true);
	return packet;
}

function responseDisassembler(response, count) {
	var packet = {};

	if (!response) {
		return;
	}

	packet.unit = response.readUInt8(0, true);
	packet.func = response.readUInt8(1, true);
	packet.byteCount = response.readUInt8(2, true);

	var bytes = response.slice(3, response.length - 2);

	packet.registers = [];

	// Two bytes per item
	for (var i=0; i < bytes.length; i+=2) {
		packet.registers.push(bytes.readUInt16BE(i, true));
	}

	return packet;

}

// Function to broadcast ECU data
function responseHandler(registers, startAddress) {
	var response = {};

	if (startAddress > 0) {
		for (var i=0; i<=registers.length; i++) {
			var register = datapoints.getDatapoints(registers[i], startAddress + i, true);
			if (register)
				_.extend(response, register);
		}
	}
	
	// Broadcast our results to the WebSocket Server
	wsServer.broadcast(JSON.stringify(response));
}


// Function to get ECU data when not connected to WARI
function fetchEcuData() {
	if (!modbusReady || !selfRequests) {
		// modbus is not ready or selfRequests are not a thing, so dont do them
		selfRequestTimer = setTimeout(fetchEcuData, 5000);
		return;
	}

	// Dont try to request more information before it's aavailable.
	// This should be a blocking function anyway, but just in case...
	if (selfRequestInProgress) {
		selfRequestTimer = setTimeout(fetchEcuData, config.interval);
	} else {
		selfRequestInProgress=true;
	}

	// Get the next request from our request list
	var thisRequest = datapoints['requests'][selfRequestPointer];

	// Get registers (// blocking - I do not really mind this.)
	var registers = modbusConnection.getRegs(thisRequest.start, thisRequest.count, true);

	if (registers) {
		if (registers.length == thisRequest.count) {
			responseHandler(registers, thisRequest.start);
		}
	}
	
	// Next time round, get the next request
	selfRequestPointer++
	
	// Reset the request pointer if necessary
	if (selfRequestPointer >= datapoints['requests'].length) {
		selfRequestPointer=0;
	}

	// Mark this request complete and continue
	selfRequestInProgress=false;

	// Trigger the next request
	selfRequestTimer = setTimeout(fetchEcuData, config.interval);

}


