/*
register dict is like this:
	registers
		-> 4096
			[
				{	"name": "IQ",
					"desc": "User"s current IQ"
					"max":
					"min":
					"size":
					"illegal":
					"math": lambda function
				 }
			]

*/


function afrMath(i) {
	return i / 10;
}

function injectorMath(i) {
	return i / 1500;
}

function ignitionMath(i) {
	return i * 0.2;
}


function extractDatapointFromRegister(registerData, registerID, doMath) {
	/* function to extract datapoints from a register

	:param registerData: register from modbus
	:param registerID: Which register we are getting data from
	:param options: Dict for extra opts
	:returns: A dict of datapoints extracted from this register
	
	*/
	var output = {}

	var register = adaptronic['registers'][registerID];
	if (!register) {
		//		console.log("Unable to decode register " + registerID);
		return false;
	}

	// Register extract logic

	// This is a pointer to our current location within the register
	var bitCount = 16

	var currentDatapoint;

	// Ok we got the register data,
	// One register can contain many datapoints

	register.forEach(function(datapoint){
		
		// The value of this data point will be stored here
		var dpValue = 0;

		if (!datapoint['size'])
			return false;

		// Pointer to our current location within the datapoint
		bitCount -= datapoint['size'];
		if (!datapoint['skip']) {

			switch (datapoint['size']) {
				case 16:
					dpValue = (registerData & ~(1 << 15)) - (registerData & (1 << 15));
					break;

				case 8:
					dpValue = registerData >> bitCount;
					dpValue = dpValue & 0xFF;
					break;

				case 1:
					dpValue = registerData >> bitCount;
					dpValue = dpValue & 1;
					break;
				default:
					console.error("I don't know how to decode a datapoint of that size mang. Srs.");
			}

		} else {
			// This datapoint is unknown or is a skip-point
		}

		// If we're doing math for this datapoint, then do math for this datapoint. // Redundant comment much?
		if (doMath == true && typeof datapoint['math'] == "function") {
			dpValue = datapoint.math(dpValue);
		}

		// Assign the value to the output object
		output[datapoint['name']] = dpValue;

	});

	// Return the datapoints inside this register
	return output;
}



adaptronic = {
	"getDatapoints": extractDatapointFromRegister,
	"requests":
	[
		{
			"start"	: 4096,
			"count" :	22,
		},
		{
			"start" : 4138,
			"count" :	17,
		},
		{
			"start" : 4194,
			"count" :	 8,
		}
	],
	"registers":
	{
		4096: 
			[
				{	"desc": "Current RPM",
					"max": 10000,
					"min": 0,
					"name": "RPM",
					"size": 16
				}
			],
		4097: 
			[	
				{	"desc": "Mass Air Pressure (kPa)",
					"max": 300,
					"min": 0,
					"name": "MAP",
					"size": 16
				}
			],
		4098: 
			[	
				{	"desc": "Manifold Air Temperature (deg C)",
					"max": 125,
					"min": -32,
					"name": "MAT",
					"size": 16
				}
			],
		4099: 
			[	
				{	"desc": "Water Temperature (dec C)",
					"max": 125,
					"min": -32,
					"name": "WT",
					"size": 16
				}
			],
		4100: 
			[	
				{	"desc": "Auxiliary Temperature (deg C)",
					"max": 125,
					"min": -32,
					"name": "AUXT",
					"size": 16
				}
			],
		4101: 
			[	
				{	"desc": "Air to Fuel Ratio (Analog)",
					"illegal": [255],
					"math": afrMath ,
					"max": 20.0,
					"min": 9.4,
					"name": "AFRA",
					"size": 8
				},
				
				{	"desc": "Air to Fuel Ratio (Actual)",
					"illegal": [255],
					"math": afrMath ,
					"max": 20.0,
					"min": 9.4,
					"name": "RAFR",
					"size": 8
				}
			],
		4102: 
			[	
				{	"desc": "Knock Value",
					"max": 255,
					"min": 0,
					"name": "KNCK",
					"size": 16
				}
			],
		4103: 
			[	
				{	"desc": "Throttle Position Sensor",
					"illegal": [255, 65535],
					"max": 100,
					"min": 0,
					"name": "TPS",
					"size": 16
				}
			],
		4104: 
			[	
				{	"desc": "Idle Duty Cycle",
					"max": 255,
					"min": 0,
					"name": "IDL",
					"size": 16
				}
			],
		4105: 
			[	
				{	"desc": "Battery Voltage",
					"max": 20,
					"min": 0,
					"name": "BATV",
					"size": 16
				}
			],
		4106: 
			[	
				{	"desc": "Master VSS (km/h)",
					"max": 220,
					"min": 0,
					"name": "MVSS",
					"size": 16
				}
			],
		4107: 
			[	
				{	"desc": "Secondary VSS (km/h)",
					"max": 220,
					"min": 0,
					"name": "SVSS",
					"size": 16
				}
			],
		4108: 
			[	
				{	"desc": "Injector 1 PulseWidth",
					"math": injectorMath,
					"max": 20,
					"min": 0,
					"name": "INJ1",
					"size": 16
				}
			],
		4109: 
			[	
				{	"desc": "Injector 2 PulseWidth",
					"math": injectorMath,
					"max": 20,
					"min": 0,
					"name": "INJ2",
					"size": 16
				}
			],
		4110: 
			[	
				{	"desc": "Injector 3 PulseWidth",
					"math": injectorMath,
					"max": 20,
					"min": 0,
					"name": "INJ3",
					"size": 16
				}
			],
		4111: 
			[	
				{	"desc": "Injector 4 PulseWidth",
					"math": injectorMath,
					"max": 20,
					"min": 0,
					"name": "INJ4",
					"size": 16
				}
			],
		4112: 
			[	
				{	"desc": "Ignition Timing 1 (Deg BTDC)",
					"math": ignitionMath,
					"max": 60,
					"min": -10,
					"name": "IGN1",
					"size": 16
				}
			],
		4113: 
			[	
				{	"desc": "Ignition Timing 2 (Deg BTDC)",
					"math": ignitionMath,
					"max": 60,
					"min": -10,
					"name": "IGN2",
					"size": 16
				}
			],
		4114: 
			[	
				{	"desc": "Ignition Timing 3 (Deg BTDC)",
					"math": ignitionMath,
					"max": 60,
					"min": -10,
					"name": "IGN3",
					"size": 16
				}
			],
		4115: 
			[	
				{	"desc": "Ignition Timing 4 (Deg BTDC)",
					"math": ignitionMath,
					"max": 60,
					"min": -10,
					"name": "IGN4",
					"size": 16
				}
			],
		4116: 
			[	
				{	"desc": "Fuel Trim (%)",
					"max": 100,
					"min": 0,
					"name": "FTRM",
					"size": 16
				}
			],
		4138: 
			[	
				{	"desc": "Target Idle RPM",
					"max": 10000,
					"min": 0,
					"name": "TIDL",
					"size": 16
				}
			],
		4139: 
			[	
				{	"desc": "Target Manifold Pressure",
					"max": 400,
					"min": 0,
					"name": "TMAP",
					"size": 16
				}
			],
		4140: 
			[	
				{	"desc": "Target Air to Fuel Ratio",
					"math": afrMath ,
					"max": 20,
					"min": 9.4,
					"name": "TAFR",
					"size": 16
				}
			],
		4141: 
			[	
				{	"desc": "Current Gear",
					"max": 10,
					"min": 0,
					"name": "GEAR",
					"size": 8
				},
				
				{	"desc": "Blow-Off-Valve State",
					"max": 255,
					"min": 0,
					"name": "BOV",
					"size": 8
				}
			],
		4142: 
			[	
				{	"desc": "Skips a certain number of bits",
					"name": "NUL",
					"size": 12,
					"skip": true
				},
				
				{	"desc": "Fuel 2 Cutting State",
					"max": 1,
					"min": 0,
					"name": "CUF2",
					"size": 1
				},
				
				{	"desc": "Fuel 1 Cutting State",
					"max": 1,
					"min": 0,
					"name": "CUF1",
					"size": 1
				},
				
				{	"desc": "Ignition 2 Cutting State",
					"max": 1,
					"min": 0,
					"name": "CUI2",
					"size": 1
				},
				
				{	"desc": "Ignition 1 Cutting State",
					"max": 1,
					"min": 0,
					"name": "CUI1",
					"size": 1
				}
			],
		4143: 
			[	
				{	"desc": "Skips a certain number of bits",
					"name": "NUL",
					"size": 7,
					"skip": true
				},
				
				{	"desc": "Clutch Flag",
					"max": 1,
					"min": 0,
					"name": "CLFL",
					"size": 1
				},
				
				{	"desc": "Idle Flag",
					"max": 1,
					"min": 0,
					"name": "IDLE",
					"size": 1
				},
				
				{	"desc": "SEND Flag?",
					"max": 1,
					"min": 0,
					"name": "SEND",
					"size": 1
				},
				
				{	"desc": "Cloth flag?",
					"max": 1,
					"min": 0,
					"name": "CLTH",
					"size": 1
				},
				
				{	"desc": "Wide Open Throttle Flag",
					"max": 1,
					"min": 0,
					"name": "WOT",
					"size": 1
				},
				
				{	"desc": "Dirty Flag",
					"max": 1,
					"min": 0,
					"name": "DIRT",
					"size": 1
				},
				
				{	"desc": "IGCR?",
					"max": 1,
					"min": 0,
					"name": "IGCR",
					"size": 1
				},
				
				{	"desc": "Cranking Flag",
					"max": 1,
					"min": 0,
					"name": "CRNK",
					"size": 1
				},
				
				{	"desc": "MULLALOO Flag?",
					"max": 1,
					"min": 0,
					"name": "MUL",
					"size": 1
				}
			],
		4144: 
			[	
				{	"desc": "Skips bits",
					"name": "NUL",
					"size": 8,
					"skip": true
				},
				
				{	"desc": "Ignition ON Flag",
					"max": 1,
					"min": 0,
					"name": "IGON",
					"size": 1
				},
				
				{	"desc": "Nitrous Oxide Flag",
					"max": 1,
					"min": 0,
					"name": "NOS",
					"size": 1
				},
				
				{	"desc": "CLOTH Flag Again?",
					"max": 1,
					"min": 0,
					"name": "CTH2",
					"size": 1
				},
				
				{	"desc": "Wide Open Throttle Flag Again",
					"max": 1,
					"min": 0,
					"name": "WOT2",
					"size": 1
				},
				
				{	"desc": "Air-Con Flag",
					"max": 1,
					"min": 0,
					"name": "AC",
					"size": 1
				},
				
				{	"desc": "Electricity Flag",
					"max": 1,
					"min": 0,
					"name": "ELEC",
					"size": 1
				},
				
				{	"desc": "Clutch Flag Again Maybe?",
					"max": 1,
					"min": 0,
					"name": "CLUT",
					"size": 1
				},
				
				{	"desc": "Ignition Flag",
					"max": 1,
					"min": 0,
					"name": "IGNF",
					"size": 1
				}
			],
		4145: 
			[	
				{	"desc": "Skips forwards more bits",
					"name": "NUL",
					"size": 13,
					"skip": true
				},
				
				{	"desc": "TurboTimer Output",
					"max": 1,
					"min": 0,
					"name": "TT",
					"size": 1
				},
				
				{	"desc": "Air-Con Output",
					"max": 1,
					"min": 0,
					"name": "AC2",
					"size": 1
				},
				
				{	"desc": "Fuel Pump Output",
					"max": 1,
					"min": 0,
					"name": "FP",
					"size": 1
				}
			],
		4146: 
			[	
				{	"desc": "Skip some bits",
					"name": "NUL",
					"size": 10,
					"skip": true
				},
				
				{	"desc": "Learning Ignition LOAD Status",
					"max": 1,
					"min": 0,
					"name": "ILOD",
					"size": 1
				},
				
				{	"desc": "Learning Ignition RPM Status",
					"max": 1,
					"min": 0,
					"name": "IRPM",
					"size": 1
				},
				
				{	"desc": "Learning Ignition WAIT Status",
					"max": 1,
					"min": 0,
					"name": "IWAT",
					"size": 1
				},
				
				{	"desc": "Learning Fuel LOAD Status",
					"max": 1,
					"min": 0,
					"name": "FLOD",
					"size": 1
				},
				
				{	"desc": "Learning Fuel RPM Status",
					"max": 1,
					"min": 0,
					"name": "FRPM",
					"size": 1
				},
				
				{	"desc": "Learning Fuel WAIT Status",
					"max": 1,
					"min": 0,
					"name": "FWAT",
					"size": 1
				}
			],
		4147: 
			[	
				{	"desc": "Skip some bytes, next datapoint is 5 points away from here",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4148: 
			[	
				{	"desc": "Skip some bytes, next datapoint is 4 points away from here",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4149: 
			[	
				{	"desc": "Skip some bytes, next datapoint is 3 points away from here",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4150: 
			[	
				{	"desc": "Skip some bytes, next datapoint is 2 points away from here",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4151: 
			[	
				{	"desc": "Skip some bytes, next datapoint is 1 points away from here",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4152: 
			[	
				{	"desc": "Wastegate Duty Cycle (Actual)",
					"max": 1,
					"min": 0,
					"name": "WGDC",
					"size": 16
				}
			],
		4194: 
			[	
				{	"desc": "VVT2 Duty Cycle",
					"max": 100,
					"min": 0,
					"name": "V2DC",
					"size": 8
				},
				
				{	"desc": "VVT2 SetPoint (deg BTDC)",
					"max": 360,
					"min": 0,
					"name": "V2SP",
					"size": 8
				}
			],
		4195: 
			[	
				{	"desc": "VVT1 Duty Cycle",
					"max": 100,
					"min": 0,
					"name": "V1DC",
					"size": 8
				},
				
				{	"desc": "VVT1 SetPoint (deg BTDC)",
					"max": 360,
					"min": 0,
					"name": "V1SP",
					"size": 8
				}
			],
		4196: 
			[	
				{	"desc": "External input value (-2000 to +2000)",
					"max": 2000,
					"min": -2000,
					"name": "EXIN",
					"size": 16
				}
			],
		4197: 
			[	
				{	"desc": "Skip bytes to get to DigInRaw",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4198: 
			[	
				{	"desc": "Skip bytes to get to DigInRaw",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4199: 
			[	
				{	"desc": "Skip bytes to get to DigInRaw",
					"name": "NUL",
					"size": 16,
					"skip": true
				}
			],
		4200: 
			[	
				{	"desc": "Skip bytes to get to DigInRaw",
					"name": "NUL",
					"size": 8,
					"skip": true
				},
				
				{	"desc": "Digital Input 7",
					"max": 1,
					"min": 0,
					"name": "DIG7",
					"size": 1
				},
				
				{	"desc": "Digital Input 6",
					"max": 1,
					"min": 0,
					"name": "DIG6",
					"size": 1
				},
				
				{	"desc": "Digital Input 5",
					"max": 1,
					"min": 0,
					"name": "DIG5",
					"size": 1
				},
				
				{	"desc": "Digital Input 4",
					"max": 1,
					"min": 0,
					"name": "DIG4",
					"size": 1
				},
				
				{	"desc": "Digital Input 3",
					"max": 1,
					"min": 0,
					"name": "DIG3",
					"size": 1
				},
				
				{	"desc": "Digital Input 2",
					"max": 1,
					"min": 0,
					"name": "DIG2",
					"size": 1
				},
				
				{	"desc": "Digital Input 1",
					"max": 1,
					"min": 0,
					"name": "DIG1",
					"size": 1
				}
			]
	}
}

module.exports = adaptronic;